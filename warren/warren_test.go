package warren

import (
	"bytes"
	"reflect"
	"testing"
)

func TestWarren_BuildBody_NoValues(t *testing.T) {
	dtos := []any{
		CreateFloatingIpRequest{},
		CreateVirtualMachineRequest{},
	}
	for _, dto := range dtos {
		// Given
		params := ApiCall{
			jsonBody: dto,
		}

		// When
		body, contentType, err := BuildBody(params)

		// Then
		if err != nil {
			t.Errorf("%+v, Error was returned from BuildBody: %+v", reflect.TypeOf(dto), err)
		}
		if contentType != "application/json; charset=utf-8" {
			t.Errorf("%+v, JSON content type was expected, instead got %+v", reflect.TypeOf(dto), contentType)
		}
		var buf bytes.Buffer
		_, err = body.WriteTo(&buf)
		if err != nil {
			t.Errorf("%+v, Error writing result to buffer: %+v", reflect.TypeOf(dto), err)
		}
		resJson := buf.String()
		if resJson != "{}" {
			t.Errorf("%+v, Empty JSON was expected, instead got %+v", reflect.TypeOf(dto), resJson)
		}
	}
}

func TestWarren_BuildBody_CreateVirtualMachine_FalseBooleans(t *testing.T) {
	// Given
	dto := CreateVirtualMachineRequest{
		Backup:          New(false),
		ReservePublicIp: New(false),
	}
	params := ApiCall{
		jsonBody: dto,
	}

	// When
	body, contentType, err := BuildBody(params)

	// Then
	if err != nil {
		t.Errorf("%+v, Error was returned from BuildBody: %+v", reflect.TypeOf(dto), err)
	}
	if contentType != "application/json; charset=utf-8" {
		t.Errorf("%+v, JSON content type was expected, instead got %+v", reflect.TypeOf(dto), contentType)
	}
	var buf bytes.Buffer
	_, err = body.WriteTo(&buf)
	if err != nil {
		t.Errorf("%+v, Error writing result to buffer: %+v", reflect.TypeOf(dto), err)
	}
	resJson := buf.String()
	if resJson != `{"backup":false,"reserve_public_ip":false}` {
		t.Errorf("%+v, JSON with `false` booleans was expected, instead got %+v", reflect.TypeOf(dto), resJson)
	}
}

func TestWarren_BuildBody_CreateFloatingIp_ZeroBillingAccountId(t *testing.T) {
	// Given
	dto := CreateFloatingIpRequest{
		BillingAccountId: New(0),
	}
	params := ApiCall{
		jsonBody: dto,
	}

	// When
	body, contentType, err := BuildBody(params)

	// Then
	if err != nil {
		t.Errorf("%+v, Error was returned from BuildBody: %+v", reflect.TypeOf(dto), err)
	}
	if contentType != "application/json; charset=utf-8" {
		t.Errorf("%+v, JSON content type was expected, instead got %+v", reflect.TypeOf(dto), contentType)
	}
	var buf bytes.Buffer
	_, err = body.WriteTo(&buf)
	if err != nil {
		t.Errorf("%+v, Error writing result to buffer: %+v", reflect.TypeOf(dto), err)
	}
	resJson := buf.String()
	if resJson != `{"billing_account_id":0}` {
		t.Errorf("%+v, JSON with `0` int was expected, instead got %+v", reflect.TypeOf(dto), resJson)
	}
}
