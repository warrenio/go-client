package main

import (
	u "gitlab.com/warrenio/library/go-client/examples/utils"
	w "gitlab.com/warrenio/library/go-client/warren"
	"log"
	"os"
	"strconv"
)

func main() {
	// Build client object
	api, err := (&w.ClientBuilder{}).
		ApiUrl(os.Getenv("API_URL")).
		ApiToken(os.Getenv("API_TOKEN")).
		LocationSlug("tll").
		Build()
	if err != nil {
		log.Fatal("Cannot build client, ", err)
	}

	BlockStorageTesting(api)

}

func BlockStorageTesting(api *w.Client) {

	billingAccountId, _ := strconv.Atoi(os.Getenv("BA_ID"))

	createdDisk, err := api.BlockStorage.CreateDisk(&w.CreateDiskRequest{
		SizeGb: w.New(20),
	})
	if err != nil {
		log.Fatalf("Failed to create disk: %s ", err)
	}
	log.Println("================================================================")
	log.Printf("Created disk :\n %s",
		u.PrettyPrintObject(createdDisk))
	log.Println("================================================================")

	diskUuid := createdDisk.Uuid
	diskByUuid, err := api.BlockStorage.GetDiskById(diskUuid)
	if err != nil {
		log.Fatalf("Failed to get disk by uuid [%s]: %s ", diskUuid, err)
	}
	log.Println("================================================================")
	log.Printf("disk by uuid [%s]:\n %s",
		diskUuid, u.PrettyPrintObject(diskByUuid))
	log.Println("================================================================")

	userDisks, err := api.BlockStorage.ListUserDisks()
	if err != nil {
		log.Fatalf("Failed to get list of user disks: %s ", err)
	}
	log.Println("================================================================")
	log.Printf("list of user disks:\n %s",
		u.PrettyPrintObject(userDisks))
	log.Println("================================================================")

	updatedDisk, err := api.BlockStorage.ChangeDiskBillingAccount(diskUuid, billingAccountId)
	if err != nil {
		log.Fatalf("Failed to update disk billing account: %s ", err)
	}
	log.Println("================================================================")
	log.Printf("changed disk billing account :\n %s",
		u.PrettyPrintObject(updatedDisk))
	log.Println("================================================================")

	err = api.BlockStorage.DeleteDiskById(diskUuid)
	if err != nil {
		log.Fatalf("Failed to delete disk: %s ", err)
	}
	log.Println("================================================================")
	log.Printf("deleted disk by id [%s]", diskUuid)
	log.Println("================================================================")

}
